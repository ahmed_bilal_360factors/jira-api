package impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import service.Api;
import util.HttpRestClient;

@Named("api")
@ExportAsService
public class PredictServiceImpl implements Api
{
	private static final Logger log = LoggerFactory.getLogger(PredictServiceImpl.class);
	private static final GroupManager groupManager = ComponentAccessor.getGroupManager();
	private static final String assigneeTypeFieldID = "customfield_12800";
	private static final String groupAssigneeFieldID = "customfield_12802";
	
	public ApplicationUser getLoggedInUser() {
		return ComponentAccessor.getComponent(JiraAuthenticationContext.class)
				.getLoggedInUser();
	}
	
	public Project getProject(){
		PermissionManager permissionManager = ComponentAccessor.getPermissionManager();
		Project userProject = null;
		// Get projects for current user with browse permission. There must
		// be one project only
		List<Project> userProjects = (List<Project>) permissionManager.getProjects(new ProjectPermissionKey(Permissions.BROWSE), getLoggedInUser());
		if (userProjects != null && userProjects.size() > 0) {
			userProject = userProjects.get(0);
		}
		return userProject;
	}
	
	public <T> T executeGet(String url,Class<?> responseEntityClass) throws Exception
	{
		return HttpRestClient.executeGet(url, responseEntityClass);	
	}

	/**
	 * assigneeType =1 Single Assignee
	 * assigneeType =2 Group Assignee
	 */
	@Override
	public boolean setIssueAssignee(MutableIssue issue, String actor, Integer assigneeType,Boolean isEvent,Boolean sendEmail) {
		IssueService issueService = ComponentAccessor.getIssueService();
		CustomField assigneeTypeField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(assigneeTypeFieldID);
		CustomField groupField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(groupAssigneeFieldID);
		if(assigneeType.equals(1)) {
			if(!actor.toLowerCase().endsWith("@" +  issue.getProjectObject().getKey().toLowerCase())) {
				actor = actor + "@" +  issue.getProjectObject().getKey();
			}
			ApplicationUser user = ComponentAccessor.getUserManager().getUserByName(actor);
	    	issue.setAssignee(ComponentAccessor.getUserManager().getUserByName(actor));
	    	issue.setCustomFieldValue(assigneeTypeField, "0");
	    	issue.setCustomFieldValue(groupField, null);
		}else if(assigneeType.equals(2)){
			if(!actor.toLowerCase().startsWith(issue.getProjectObject().getKey().toLowerCase() + "-")) {
				actor = issue.getProjectObject().getKey() + "-" + actor;
			}
	    	issue.setAssignee(null);
	    	issue.setCustomFieldValue(assigneeTypeField, "1");
	    	ArrayList<Group>  immutableGroup = new ArrayList<Group>();
	    	immutableGroup.add(ComponentAccessor.getGroupManager().getGroup(actor));
	    	issue.setCustomFieldValue(groupField, immutableGroup);	    	
		}
		
    	IssueManager issueManager = ComponentAccessor.getIssueManager();
    	issueManager.updateIssue(getLoggedInUser(), issue,EventDispatchOption.ISSUE_ASSIGNED, true);
    	return true;
	}

	@Override
	public Boolean executePut(String url, Object putObject) throws Exception {
		return HttpRestClient.executePut(url, putObject);
	}
	
	@Override
    public FieldConfigScheme getSchemeByCustomFieldId(CustomField customField) throws Exception {
    	FieldConfigScheme configScheme = ComponentAccessor.getFieldConfigSchemeManager().getRelevantConfigScheme(getProject(), customField);
    	if(configScheme == null) {
    		String errorMsg = new Date()+" In getSchemIdByCustomFieldId >> no custom field specific scheme found for id: "+customField.getId();
    		System.out.println(errorMsg);
    		throw new NullPointerException(errorMsg);
    	}
    	return configScheme;
    }

	@Override
	public boolean isLoginUserIssueAssignee(Issue issue) {
		CustomField assigneeTypeCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(assigneeTypeFieldID);
		CustomField groupCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(groupAssigneeFieldID);
		System.out.println(assigneeTypeCF);
		if(assigneeTypeCF != null) {
			String selectedCustomFieldValue =  (String) issue.getCustomFieldValue(assigneeTypeCF);
			System.out.println("Value = " + selectedCustomFieldValue );
			if(selectedCustomFieldValue.equals("0")){
				return issue.getAssignee().getUsername().equals(getLoggedInUser().getUsername());
			}else if(selectedCustomFieldValue.equals("1")){
				ArrayList<Group> groups = (ArrayList<Group>)issue.getCustomFieldValue(groupCF);
				Group selectedGroup = groups.get(0);
				return groupManager.isUserInGroup(getLoggedInUser(), selectedGroup);
			}
		}
		return false;
	}
}	
	
