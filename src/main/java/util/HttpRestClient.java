package util;

import org.apache.commons.httpclient.HttpStatus;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class HttpRestClient {
	
	@SuppressWarnings("unchecked")
	public static final <T> T executeGet(String url,Class<?> responseEntityClass) throws Exception {
		Client client = Client.create();
        ClientResponse response = null;
		WebResource webResource = client.resource(url);
        try{
        	response = webResource.type("application/json").accept("application/json").get(ClientResponse.class);
        }catch(Exception e){
        	System.out.println("Unable to call api service =  " + e.getMessage());
        	return null;
        }
        
        if(response.getStatus() == HttpStatus.SC_OK) {
        	String jsonResponse = response.getEntity(String.class);
            System.out.println(jsonResponse);
        	Gson gson = new Gson();
			return  (T) gson.fromJson(jsonResponse, responseEntityClass);
        }else {
        	System.out.println(response.toString());
        	System.out.println(response.getStatus());
        }
        return null;
	}

	public static Boolean executePut(String url, Object putObject) {
    	Gson gson = new Gson();
		Client client = Client.create();
        ClientResponse response = null;
		WebResource webResource = client.resource(url);
        try{
        	String inputJson = gson.toJson(putObject); 
        	response = webResource.type("application/json").accept("application/json").put(ClientResponse.class,inputJson);
        }catch(Exception e){
        	System.out.println("Unable to call api service =  " + e.getMessage());
        	return false;
        }
        
        if(response.getStatus() == HttpStatus.SC_OK) {
        	String jsonResponse = response.getEntity(String.class);
            System.out.println(jsonResponse);
			return  true;
        }else {
        	System.out.println(response.toString());
        	System.out.println(response.getStatus());
        }
        return false;		
	}

}
