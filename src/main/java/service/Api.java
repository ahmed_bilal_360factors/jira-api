package service;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

public interface Api {
	ApplicationUser getLoggedInUser();
	Project getProject();
	public <T> T executeGet(String url,Class<?> responseEntityClass) throws Exception;
	public Boolean executePut(String url,Object putObject) throws Exception;
	boolean setIssueAssignee(MutableIssue issue , String actor, Integer assigneeType,Boolean isEvent,Boolean sendEmail);
	FieldConfigScheme getSchemeByCustomFieldId(CustomField customField) throws Exception;
	public boolean isLoginUserIssueAssignee(Issue issue);
}
